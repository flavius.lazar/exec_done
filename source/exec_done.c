/*
 * exec_done.c
 *
 *  Created on: May 11, 2017
 *      Author: flavius
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <stdlib.h>

void do_exec() {
	int control_pfd[2];

	if (pipe2(control_pfd, O_CLOEXEC) < 0) {
		perror("Error creating control pipes");
		exit(1);
	}

	pid_t pid = fork();
	if (pid < 0) {
		perror("Error at fork");
		exit(1);
	}
	else if (pid == 0) {
		// close reading end
		if (close(control_pfd[0]) < 0) {
			perror("Error closing reading end of control fd");
			exit(1);
		}

		if (execl("child_proc.sh", "child_proc.sh", NULL) < 0) {
			perror("Error launching child_proc.sh");
			exit(1);
		}
	}
	else {
		// close writing end
		if (close(control_pfd[1]) < 0) {
			perror("Error closing writing end of control fd");
			exit(1);
		}

		char c;
		// used to check execl status !!!
		// writing end of pipe will be closed if execl launches new process
		// or if it fails and error handler stops child process
		// should always return 0
		if (read(control_pfd[0], &c, 1) < 0) {
			perror("Error reading from control fd");
			exit(1);
		}

		int status;
		if (waitpid(pid, &status, 0) < 0) {
			perror("Error at waitpid");
			exit(1);
		}

		if (WIFEXITED(status)) {
			printf("Child process exited - status=%d\n", WEXITSTATUS(status));
		}
		else if (WIFSIGNALED(status)) {
			printf("Child process killed by signal %d\n", WTERMSIG(status));
		}
	}
}

int main(int argc, char **argv) {
	do_exec();
	return 0;
}
